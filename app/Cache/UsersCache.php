<?php
namespace App\Cache;
use App\Models\User;
use Facades\App\Cache\CacheMaster;

class UsersCache{

	CONST CACHE_KEY = 'CUSTOMERS';
	
    public function getUsers(){
        return User::all();
    }    

    public function getUserPaginate(){
        return User::paginate(10);
    }    

    public function countUsers(){
        return count($this->getUsers());
    }  

	public function reset(){
		CacheMaster::flushWithTag(self::CACHE_KEY);
	}


}