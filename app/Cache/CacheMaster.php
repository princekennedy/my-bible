<?php

namespace App\Cache;
use Carbon\Carbon;
use App\User;
use Cache;
/**
 * 
 */
class CacheMaster
{
	
	public const EXPIRE_TIME = 50;
	public const CACHE_KEYS = 'CACHE_KEYS';

	public function getCacheKey($mainKey, $key){
		return strtoupper($mainKey . '.' . $key);
	}

	public function set( $key, $callBack = null){
		$this->tag($key);
		return Cache::remember( $key, Carbon::now()->addMinutes( CacheMaster::EXPIRE_TIME ), $callBack); 
	}

	public function tag($key){
		if(cache()->has($key)) return false;
		$cacheKeys = cache()->get(self::CACHE_KEYS);
		$cacheKeys = ($cacheKeys) ? $cacheKeys : [];
		$cacheKeys[] = $key;
		$this->forget(self::CACHE_KEYS);
		Cache::remember( 
			self::CACHE_KEYS, 
			Carbon::now()->addMinutes( CacheMaster::EXPIRE_TIME ), 
			function() use ($cacheKeys) {
				return $cacheKeys;
			}
		);
	}

	public function get($key){
		return cache()->get($key);
	}

	public function forget($key){
		Cache::forget(trim($key));
		return true;
	}	

	public function flush(){
		cache()->flush();
		return true;
	}

	public function flushWithTag($tag){
		$cacheKeys = cache()->get(self::CACHE_KEYS);
		$cacheKeys = ($cacheKeys) ? $cacheKeys : [];

		foreach ($cacheKeys as $index => $key) {
			if( str_contains($key, $tag) ) {
				$this->forget($key);
				unset($cacheKeys[$index]);
			}
		}

		$this->forget(self::CACHE_KEYS);
		Cache::remember( 
			self::CACHE_KEYS, 
			Carbon::now()->addMinutes( CacheMaster::EXPIRE_TIME ), 
			function() use ($cacheKeys) {
				return $cacheKeys;
			}
		);
		return true;
	}

}
