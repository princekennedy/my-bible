<?php
namespace App\Cache;
use App\Models\Bible;
use DB;
use Facades\App\Cache\CacheMaster;
class BibleCache
{

    public $KJV  = 23146;
    public $CRTB = 23146;
    public $ASV  = 23146;
    public $NRT  = 23146;
    public $EST  = 23146;

    CONST CACHE_KEY = 'BIBLE';

	public function getBooks($trans = "KJV"){
		$operator = ( auth()->user()->getConfig("old") ) ? "<" : ">=" ;
		$boundary = $this->$trans;
		$cacheKey = CacheMaster::getCacheKey(SELF::CACHE_KEY, $this->$trans. $boundary . $operator);
		return CacheMaster::set($cacheKey, function() use ($operator, $boundary) {
			$books = Bible::where("id" , $operator , $boundary)
					->groupBy("book_name")
					->orderBy("id")
					->get();
			return $books;
		});
	}

	public function getChapters(){
		$activeBook = $this->getActiveBook();
		$book_id = ($activeBook) ? $activeBook->book_id : null;

		$cacheKey = CacheMaster::getCacheKey(SELF::CACHE_KEY, $book_id);
		return CacheMaster::set($cacheKey, function() use ($book_id) {
			$chapters = Bible::where("book_id", $book_id)
						->groupBy("chapter")
						->orderBy("id")	
						->get();
			return $chapters;
		});
	}

	public function getVerses(){
		$activeChapter = $this->getActiveChapter();
		$activeBook = $this->getActiveBook();
		$chapter = ($activeChapter) ? $activeChapter->chapter : null;

		// $cacheKey = CacheMaster::getCacheKey(SELF::CACHE_KEY, $chapter);
		// return CacheMaster::set($cacheKey, function() use ($chapter) {
			$verses = Bible::where("chapter", $chapter)
					->where("book_id", $activeBook->book_id)
					->orderBy("id")
					->get();
			return $verses;
		// });
	}

	public function getActiveBook(){
		return Bible::where("id", auth()->user()->getConfig("book_id"))->first();
	}

	public function getActiveChapter(){
		return Bible::where("chapter", auth()->user()->getConfig("chapter"))->first();
	}

	public function reset(){
		CacheMaster::flushWithTag(self::CACHE_KEY);
	}

}