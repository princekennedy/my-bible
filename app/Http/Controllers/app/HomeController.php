<?php

namespace App\Http\Controllers\app;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;
use Facades\App\Cache\UsersCache;

class HomeController extends Controller
{

    public function index(){
        return Inertia('home');
    }
}

// dd(request()->getClientIp());
// $ip = request()->ip();
// $ip = '196.216.9.155'; //For static IP address get
// $location = Location::get($ip); 