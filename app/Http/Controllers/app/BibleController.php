<?php

namespace App\Http\Controllers\app;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bible;
use Facades\App\Cache\BibleCache;

class BibleController extends Controller
{

    public function oldTestament(){
        auth()->user()->setConfig(["key" => "old", "value" => true]);
        $books = BibleCache::getBooks();
        $chapters = BibleCache::getChapters();
        $verses = BibleCache::getVerses();
        return Inertia('old-testament', compact("books", "chapters", "verses") );
    }

    public function newTestament(){
        auth()->user()->setConfig(["key" => "old", "value" => false]);
        $books = BibleCache::getBooks();
        $chapters = BibleCache::getChapters();
        $verses = BibleCache::getVerses();
        return Inertia('new-testament', compact("books", "chapters", "verses") );
    }

    public function setConfig(Request $request){
        auth()->user()->setConfig(["key" => $request->key,"value" => $request->value], $request->delete);
        return redirect()->back();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function seed($key){

        $jsonFiles = [
            "kjv" => "kjv.json",
            "rst" => "rst.json",
            "nrt" => "nrt.json",
            "crtb" => "crtb.json",
            "asv" => "asv.json",
        ];
        if(Bible::where("translation_id", $key)->first()) return "Translation $key alreay available";

        foreach ($jsonFiles as $key => $name) {

            $fileName = "json/" . $name ;
            $json = file_get_contents($fileName);
            $json = json_decode($json, false);
            $sql ="";
             // "INSERT INTO `bible` (`chapter`, `verse`, `text`, `translation_id`, `book_id`, `book_name`) VALUES ";
            foreach ($json as $keyRow => $row) {
                $sql .=  "INSERT INTO `bible` (`chapter`, `verse`, `text`, `translation_id`, `book_id`, `book_name`) VALUES ";
                $params = _array($row);
                $sql .= " ( ";
                foreach ($params as $keyParam => $value) {
                    $sql .= "'" . $this->strWork($value) . "'";
                    if($keyParam != "book_name") $sql .= ","; 
                }
                $sql .= " ); \n ";
                // if($keyRow < count($json) -1) $sql .= ", \n"; 
            }
            file_put_contents("sql/". $key . ".sql", $sql . ";");

        }
        // SOURCE sql/kjv.sql; SOURCE sql/asv.sql; SOURCE sql/crtb.sql; SOURCE sql/nrt.sql; SOURCE sql/rst.sql

        return "Seed successfully ";
    }

    public function strWork($str){
        $str = str_replace("'", "-", $str);
        return $str;
    }

}
