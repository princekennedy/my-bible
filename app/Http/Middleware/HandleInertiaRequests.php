<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Inertia\Middleware;
use Facades\App\Cache\BibleCache;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            // Synchronously
            'appName' => config('app.name'),

            // Active
            'active_book' => (auth()->check()) ? BibleCache::getActiveBook() : null ,
            'active_chapter' => (auth()->check()) ? BibleCache::getActiveChapter() : null,

            // Flash message
            'flash' => session('message', null),

            // Message
            'message' => session('message', null),

            // Error
            'error' => session('error', null),

            // Lazily
            'auth.user' => ($request->user()) ? $request->user() : null ,

        ]);
    }
}