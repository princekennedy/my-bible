<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait FilterModelByTranslationTrait
{

    public static function bootFilterModelByTranslationTrait()
    {

	    if (auth()->check()) {
            static::addGlobalScope(function (Builder $builder)  {
                $builder->where('translation_id', "KJV"); //->orWhereNull($field);
            });
      //       static::creating(function ($model) use ($business) {
		    //     $model->business_id = $business->id;
		    // });
      //       static::updating(function ($model) use ($business) {
		    //     $model->business_id = $business->id;
		    // });
        }
    }

}
