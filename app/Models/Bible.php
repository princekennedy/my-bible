<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\FilterModelByTranslationTrait;

class Bible extends Model
{
    use HasFactory, FilterModelByTranslationTrait;

    protected $table = "bible";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'id', 'chapter', 'verse','text',
        'translation_id','book_id','book_name',
        'deleted_at', 'created_at', 'updated_at', 
    ];

    public function chapters(){
        return $this->hasMany(Bible::class, "book_id");
    }
}
