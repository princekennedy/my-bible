<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [App\Http\Controllers\app\UserController::class, 'login'])->name("login"); 
Route::post('/login', [App\Http\Controllers\app\UserController::class, 'loginUser']); 
Route::get('/register', [App\Http\Controllers\app\UserController::class, 'register']); 
Route::post('/register', [App\Http\Controllers\app\UserController::class, 'store']); 

Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    
    // Users
    Route::resource('users', App\Http\Controllers\app\UserController::class); 
    Route::post('/delete-user/{user}', [App\Http\Controllers\app\UserController::class, "delete"]); 
    Route::post('/update-user/{user}', [App\Http\Controllers\app\UserController::class, "update"]); 

    // logout 
    Route::get('/logout', [App\Http\Controllers\app\UserController::class, 'logout']); 
    Route::post('/logout', [App\Http\Controllers\app\UserController::class, 'logout']); 

    Route::get('/', function(){
        return redirect('/home');
    }); 
    Route::get('/home', [App\Http\Controllers\app\HomeController::class, 'index']); 

    // Places
    Route::get('/new', [App\Http\Controllers\app\BibleController::class, 'newTestament']); 
    Route::get('/old', [App\Http\Controllers\app\BibleController::class, 'oldTestament']); 
    Route::get('/writings', [App\Http\Controllers\app\BibleController::class, 'oldTestament']); 

    Route::get('/chapters/{bible}', [App\Http\Controllers\app\BibleController::class, 'getChapters']); 
    Route::post('/set-config', [App\Http\Controllers\app\BibleController::class, 'setConfig']); 

    Route::get('/seed/{key}', [App\Http\Controllers\app\BibleController::class, 'seed']); 

});
