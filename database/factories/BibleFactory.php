<?php

namespace Database\Factories;

use App\Models\Bible;
use Illuminate\Database\Eloquent\Factories\Factory;

class BibleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bible::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
