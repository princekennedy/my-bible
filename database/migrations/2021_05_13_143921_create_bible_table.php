<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBibleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bible', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chapter');
            $table->string('verse');
            $table->text('text');
            $table->string('translation_id');
            $table->string('book_id');
            $table->string('book_name');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bible');
    }
}
